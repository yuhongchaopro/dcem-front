/**
 * Utils
 */
import request from '@/utils/http'// 导入http中创建的axios实例
// import qs from 'qs'; // 根据需求是否导入qs模块 

const utils = {
    send_email(data) {
        return request({
            url: '/user/send_dynamics_code_to_email/',
            method: 'post',
            data: data,
        })
    },

    login(data) {
        return request({
            url: '/user/login/',
            method: 'post',
            data: data,
        })
    },

    // 其他接口…………
}
export default utils;