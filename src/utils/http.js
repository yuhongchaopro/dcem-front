import axios from 'axios';
import router from '../router';
import { Notification } from 'element-ui';



axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8'


const service = axios.create({
    baseURL: process.env.VUE_APP_BASE_URL,
    withCredentials: false, // send cookies when cross-domain services
    timeout: 1000 * 5 // 请求超时
})


// 请求拦截器
service.interceptors.request.use(
    config => {

        const storage = window.localStorage
        if (!storage) {
            Notification.error({ duration: 5000, title: "您的浏览器不支持存储功能，请更换浏览器。", message: "" })
            return
        }

        const token = storage.getItem("dcem_token")

        if (config.url !== "/user/send_dynamics_code_to_email/") {
            if (token) {
                config.headers.Authorization = token;
            } else {
                Notification.error({ duration: 5000, title: "请登录。", message: "点击发送验证码按钮，使用邮箱验证码进行登录。" })
                router.replace({
                    path: '/login',
                    query: {
                        redirect: router.currentRoute.fullPath
                    }
                })
                return
            }
        }

        return config
    },
    error => {
        console.log(error) // for debug
        return Promise.reject(error)
    }
)

// 响应拦截器
service.interceptors.response.use(
    response => {
        if (response.status.toString().startsWith('2')) {   // 状态码2开头的请求进行处理，否则的话抛出错误。 
            switch (response.status.toString()) {
                case "204":
                    Notification.success({ duration: 5000, title: "删除成功", message: "" })
                    break;
                case "201":
                    // Notification.success({duration: 5000,title: "新增成功",message: ""})
                    break;
                default:
            }
            return Promise.resolve(response);
        } else {
            return Promise.reject(response);
        }
    },

    error => {
        if (error.response.status) {
            switch (error.response.status) {
                // 401: 未登录
                // 未登录则跳转登录页面，并携带当前页面的路径。                
                case 401:
                    router.replace({
                        path: '/login',
                        query: {
                            redirect: router.currentRoute.fullPath
                        }
                    });
                    break;
                // 403 token过期或者不合法
                // 跳转登录页面                
                case 403:
                    Notification.error({ duration: 5000, title: "请求不合法，请重新登录", message: error.response.data.detail })
                    router.replace({
                        path: '/login',
                        query: {
                            redirect: router.currentRoute.fullPath
                        }
                    })
                    break;
                case 404:
                    Notification.error({ duration: 5000, title: "请求路径不存在", message: "请求路径不存在" })
                    break;
                case 400:
                    // drf框架非法请求均是400，所以需要细分。TODO: 细分
                    Notification.error({ duration: 5000, title: "请求参数错误", message: error.response.data })
                    break;
                case 405:
                    Notification.error({ duration: 5000, title: "请求方法错误。", message: "" })
                    break;
                // 其他错误，直接抛出错误提示
                default:
                    Notification.error({ duration: 5000, title: "其他错误，请联系管理员处理。", message: error.response.data })
            }
            return Promise.reject(error.response);

        } else {
            Notification.error({ duration: 5000, title: "某接口超时，请联系管理员处理。", message: error.response.data })
        }
    });

// 导出实例
export default service;