import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// vuex的执行顺序是，this.$store.dispatch()触发 actions中的方法，acitons中的方法触发mutations中的方法，mutations中的方法操作state中的数据。

const actions = {
  clearToken(context){
    context.commit('clearToken')
  }
}


const mutations = {
  clearToken(state){  
    state.userInfo.token = ''
  }
}

const state = {
  userInfo: {}
  

}
const modules = {}

export default new Vuex.Store({
  state,
  mutations,
  actions,
  modules
})
