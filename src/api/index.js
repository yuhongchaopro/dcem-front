// 用户模块接口
import user from '@/api/user';
import admin from '@/api/admin';
import utils from '@/api/utils';
import monitorData from '@/api/monitorData';

// 导出接口
export default {    
    user,
    admin,
    utils,
    monitorData,
    // ……
}