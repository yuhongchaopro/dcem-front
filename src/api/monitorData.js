/**
 * monitorDatas
 */
import request from '@/utils/http'// 导入http中创建的axios实例
// import qs from 'qs'; // 根据需求是否导入qs模块 

const monitorData = {
    all () {
        return request({
            url: '/monitorData/',
            method: 'get',
        })
    },
    add (data) {
        return request({
            url: '/monitorData/',
            method: 'post',
            data: data,
        })
    },

    del (data) {
        return request({
            url: '/monitorData/' + String(data.id) + '/',
            method: 'delete',
        })
    },

    update (data) {
        return request({
            url: '/monitorData/' + String(data.id) + '/',
            method: 'put',
            data: data
        })
    },

    special_search (data) {
        return request({
            url: '/monitorData/special_search/',
            method: 'post',
            data: data,
        })
    },

    // 其他接口…………
}
export default monitorData;