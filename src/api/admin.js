/**
 * Admin
 */
import request from '@/utils/http'// 导入http中创建的axios实例
// import qs from 'qs'; // 根据需求是否导入qs模块 

const admin = {    
    adminConfig () {
        return request({
            url:'/admin/',
            method:'get',
           })  
    },

}
export default admin;